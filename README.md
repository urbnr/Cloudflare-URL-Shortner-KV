# Cloudflare URL Shortner KV



## Getting started

Make sure you've a paid Cloudflare Workers account as free account will hit limits in this project.

## Make NameSpace KV and Bind it to Worker

- Create a Name Space in KV
- Bind it in Varibale to the Worker and name the Varibale `SHORTNER`
- Copy and paste the script and add your own API Key (anything you want to make it secure), Telegram Bot Token and Channel ID (for Logs). 

## Improve this project

- Have ideas? Help improve this project

## License

- MIT
