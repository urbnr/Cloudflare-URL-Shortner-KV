const SHORT_URLS = SHORTNER;
const html = `{"status" : "200", "about" : "This is a URL Shortener Service provided by Hash Hacker's" }`;
const main_apikey = "";

async function handleRequest(request) {
  function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
  var generated_code = makeid(6);
  var fetch_link = await SHORT_URLS.get(generated_code);
  if (fetch_link != null) { var generated_code = makeid(6); }
  var url = new URL(request.url);
  var path = url.pathname;
  var hostname = url.hostname;
  const { searchParams } = url
  var query = searchParams.get('url');
  var apikey = searchParams.get('apikey');
  let redirect_link
  let stampDate = Date.now();
  let localDate = new Date (stampDate).toLocaleString('en-IN', {hour12: true , timeZone: 'Asia/Kolkata'});
  const LOG_URL = "https://api.telegram.org/TelegramBotToken/sendMessage?chat_id=-100XXXXXXXXX&text=Time: "+localDate;

  if (path == '/api' && apikey == main_apikey && query != '') {
        if (query.startsWith("https://") || query.startsWith("http://")) {
           await SHORT_URLS.put(generated_code, query); 

        } else {
           var query = "https://" + query;
           await SHORT_URLS.put(generated_code, query);
        }
            telegram = await fetch(LOG_URL+'\n\nLink: https://bhadoo.eu.org/'+generated_code+'\n\nRedirect to: '+query, {
                    method: "GET",
                    //body: data,
            })
        return new Response('{ "url" : "'+query+'", "short_url" : "https://bhadoo.eu.org/'+generated_code+'", "status" : "200" }', {
            status: 200,
            headers: {
                "content-type": "application/json",
                "Access-Control-Allow-Origin": '*',
                "Cache-Control": "max-age=86400",
            },
            })
  }
  else if (path.startsWith('/') && path != '/') {
      redirect_link  = await SHORT_URLS.get(path.slice(1));
      if (redirect_link != null) {
            try {
            return Response.redirect(redirect_link, 301);
            }
            catch {
                return new Response('{ "status" : "404", "reason": "Unable to Find a Valid Link to Redirect" }', {
                status: 500,
                headers: {
                    'content-type': 'application/json;charset=UTF-8',
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Methods": "GET,HEAD,POST,OPTIONS",
                },
                });   
            }
      }
      else {
            return new Response('{ "status" : "404" }', {
            status: 500,
            headers: {
                'content-type': 'application/json;charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET,HEAD,POST,OPTIONS",
            },
            }); 
      }
  } else {
    return new Response(html, {
      status: 200,
      headers: {
        'content-type': 'application/json;charset=UTF-8',
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "GET,HEAD,POST,OPTIONS",
      },
    });
  }
}

addEventListener('fetch', event => {
  return event.respondWith(handleRequest(event.request));
});
